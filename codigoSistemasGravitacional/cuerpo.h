#ifndef CUERPO_H
#define CUERPO_H
#include <math.h>
#include <vector>

using namespace std;


class cuerpo
{
private:
    float posX,posY,masa,radio,velX,velY,G,aceleracionX, aceleracionY;
public:
    cuerpo(float _posX, float _posY, float _masa, float _radio, float _velX, float _velY, float _G=1, float _aceleracionX=0, float _aceleracionY=0);
    void actualizarAceleracion(vector <cuerpo> &listaObjetos, int posicion, float dt);

    float get_posX(){return posX;}
    float get_posY(){return posY;}
    float get_masa(){return masa;}
    float get_G(){return G;}
};

#endif // CUERPO_H
