#include <iostream>
#include <cuerpo.h>
#include <fstream>
#include <vector>
#include <math.h>
#include <stdlib.h>

using namespace std;

int main()
{
    int nCuerpos;
    float deltaTiempo = 1.0;
    ofstream archivo("ejemplo.txt");

    do{
        cout <<"Ingrese cuantos cuerpos desea ingresar?"<<endl;
        cin >>nCuerpos;
        if (nCuerpos<2){
            cout<<"Ingrese DOS o mas cuerpos. Ingrese de nuevo."<<endl;
        }
    }while(nCuerpos<2);
        string nombreCuerpo;
        float posX, posY, masa, radio, velX, velY;
        vector <cuerpo> listaObjetos;
        for (int i=0;i<nCuerpos;i++){
            cout <<"Ingrese el nombre del cuerpo: "<<endl;
            cin >>nombreCuerpo;
            cout <<"Ingrese la posicion X inicial: "<<endl;
            cin >> posX;
            cout <<"Ingrese la posicion Y inicial: "<<endl;
            cin >> posY;
            cout <<"Ingrese la masa: "<<endl;
            cin >> masa;
            cout <<"Ingrese el radio: "<<endl;
            cin >> radio;
            cout <<"Ingrese la velocidad X inicial: "<<endl;
            cin >> velX;
            cout <<"Ingrese la velocidad Y inicial: "<<endl;
            cin >> velY;

            cuerpo nombreCuerpo(posX, posY, masa, radio, velX, velY);
            listaObjetos.push_back(nombreCuerpo);
            system("CLS");  //Limpia la consola

        }


        //imprimir posicionX y posicionY de los cuerpos

            for (int i=0;i<10000;i++){
                for (int itVector=0; itVector != listaObjetos.size(); itVector++){
                    archivo<< listaObjetos[itVector].get_posX()<<"\t"<<listaObjetos[itVector].get_posY()<<'\t';
                    listaObjetos[itVector].actualizarAceleracion(listaObjetos, itVector, deltaTiempo);

                }
                archivo<<'\n';


            }


        archivo.close();
        cout << "Se ha guardado en el archivo ejemplo.txt exitosamente." << endl;

    return 0;
}
