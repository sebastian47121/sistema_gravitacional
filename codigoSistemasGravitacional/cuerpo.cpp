#include "cuerpo.h"

cuerpo::cuerpo(float _posX, float _posY, float _masa, float _radio, float _velX, float _velY, float _G, float _aceleracionX, float _aceleracionY)
{
    posX=_posX;
    posY=_posY;
    masa=_masa;
    radio=_radio;
    velX=_velX;
    velY=_velY;
    G=_G;
    aceleracionX=_aceleracionX;
    aceleracionY=_aceleracionY;
}
void cuerpo::actualizarAceleracion(vector <cuerpo> &listaObjetos, int posicion, float dt){
    float r,angulo;
    aceleracionX=0;
    aceleracionY=0;


        for (int itVector2=0; itVector2!=listaObjetos.size(); itVector2++){
            if (itVector2!=posicion){
                r=sqrt(pow(listaObjetos[itVector2].get_posX()-listaObjetos[posicion].get_posX(),2)+pow(listaObjetos[itVector2].get_posY()-listaObjetos[posicion].get_posY(),2));
                angulo=atan2((listaObjetos[itVector2].get_posY()-listaObjetos[posicion].get_posY()),(listaObjetos[itVector2].get_posX()-listaObjetos[posicion].get_posX()));
                aceleracionX+= listaObjetos[posicion].get_G()*listaObjetos[itVector2].get_masa()/pow(r,2)*cos(angulo);
                /*
                cout <<"angulo: "<<angulo<<endl;

                cout <<"aceleracionX cuerpo"<<itVector<<": "<<listaObjetos[itVector].aceleracionX<<endl;
                cout << "get_G: "<<listaObjetos[itVector].get_G()<<endl;
                cout <<"get_masa2: "<<listaObjetos[itVector2].get_masa()<<endl;
                cout <<"cos(angulo): "<<cos(angulo)<<endl;
                cout <<"sin(angulo): "<<sin(angulo)<<endl<<endl<<endl;  */
                aceleracionY+= listaObjetos[posicion].get_G()*listaObjetos[itVector2].get_masa()/pow(r,2)*sin(angulo);
                //cout <<"aceleracionY cuerpo"<<itVector<<": "<<listaObjetos[itVector].aceleracionY<<endl;


        }
    }
        listaObjetos[posicion].posX= posX+velX*dt +aceleracionX * dt*dt/2;
        listaObjetos[posicion].posY= posY+velY*dt +aceleracionY * dt*dt/2;
        velX= velX + aceleracionX*dt;
        velY= velY + aceleracionY*dt;
}


